# Use Neo4j on genobioinfo

## Install 

download here 
```
cd /home/cguyomar/work/soft/neo4j
wget https://neo4j.com/artifact.php?name=neo4j-community-5.16.0-unix.tar.gz
tar -xvf artifact.php\?name\=neo4j-community-5.16.0-unix.tar.gz
cd neo4j-community-5.16.0

neo4j_bin=/home/cguyomar/work/soft/neo4j/neo4j-community-5.16.0/bin/neo4j
neo4j_admin=/home/cguyomar/work/soft/neo4j/neo4j-community-5.16.0/bin/neo4j-admin
neo4j_cypher=/home/cguyomar/work/soft/neo4j/neo4j-community-5.16.0/bin/cypher-shell
```

## Test

From this : `https://neo4j.com/docs/operations-manual/current/tutorial/neo4j-admin-import/`

```
cd /home/cguyomar/work/multireg/neo4j_test

``` 

### Get toy data



`movies.csv`
```
movieId:ID,title,year:int,:LABEL
tt0133093,"The Matrix",1999,Movie
tt0234215,"The Matrix Reloaded",2003,Movie;Sequel
tt0242653,"The Matrix Revolutions",2003,Movie;Sequel
```


`actors.csv`
```
personId:ID,name,:LABEL
keanu,"Keanu Reeves",Actor
laurence,"Laurence Fishburne",Actor
carrieanne,"Carrie-Anne Moss",Actor
```

`roles.csv\
```
:START_ID,role,:END_ID,:TYPE
keanu,"Neo",tt0133093,ACTED_IN
keanu,"Neo",tt0234215,ACTED_IN
keanu,"Neo",tt0242653,ACTED_IN
laurence,"Morpheus",tt0133093,ACTED_IN
laurence,"Morpheus",tt0234215,ACTED_IN
laurence,"Morpheus",tt0242653,ACTED_IN
carrieanne,"Trinity",tt0133093,ACTED_IN
carrieanne,"Trinity",tt0234215,ACTED_IN
carrieanne,"Trinity",tt0242653,ACTED_IN
```$


 ### Create graph
 
 ```
 $neo4j_admin database import full --nodes=data/movies.csv --nodes=data/actors.csv --relationships=data/roles.csv neo4j
 ```

For some reason I had to change the default password (neo4j)
```
$neo4j_admin dbms set-initial-password multireg
```

### Query

```
$neo4j_bin start
$neo4j_cypher --database=neo4j -u neo4j -p multireg "MATCH (n) RETURN count(n) as nodes"
```
