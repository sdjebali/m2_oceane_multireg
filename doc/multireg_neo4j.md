# Create multireg database on Neo4j

## Installation

Prerequisites:

```bash
srun --mem=64G --pty bash
source ~fragencode/tools/modules.genologin.load.2023-11-14.sh
#module load devel/java/17.0.6
```

To download neo4j-community-5.17.0 version on genobioinfo:

```bash
mkdir /home/ocarpentier/work/soft/neo4j
cd /home/ocarpentier/work/soft/neo4j
wget https://neo4j.com/artifact.php?name=neo4j-community-5.17.0-unix.tar.gz
tar -xvf artifact.php\?name\=neo4j-community-5.17.0-unix.tar.gz
```
To make neo4j easier to use (do it the first time only):

```bash
echo export NEO4J_HOME=/home/ocarpentier/work/soft/neo4j/neo4j-community-5.17.0 >> ~/.bashrc
echo export PATH=\$PATH:/home/ocarpentier/work/soft/neo4j/neo4j-community-5.17.0/bin >> ~/.bashrc
source ~/.bashrc
``` 

Creation of a directory containing data for the multireg database:

```bash
dir=/home/ocarpentier/work/soft/neo4j/multireg
mkdir $dir
```

## File preparation

### Nodes files

#### Peak 

Create file for importation in Neo4j:
```bash
peaks=/work/project/fragencode/workspace/geneswitch/results/atacseq/gallus_gallus/nf-core.atacseq.v2.1.2.GRCg6a.108.23-12-15/bwa/merged_replicate/macs2/broad_peak/consensus/consensus_peaks.mRp.clN.bed
cut -f -4 $peaks | awk -F, 'BEGIN{OFS="\t"} {print $0, "Peak"}' | sed 's/\t/,/g' > $dir/nodes_peaks_for_import.csv
```

Add a header: `chromosome,start:int,end:int,name:ID,:LABEL`

```bash
sed -i '1ichromosome,start:int,end:int,name:ID,:LABEL' $dir/nodes_peaks_for_import.csv
```

Ouput file:

CSV containing:
- Chromosome
- Start
- End
- Name
- Label

#### Tagiso genes


Select only gene and create file fort importation in Neo4j:
```bash
tagiso=/work/project/fragencode/workspace/geneswitch/results/rnaseq/gallus_gallus/nf-feelnc.tagiso.23-03-03/results/annotation/novel.clean.withgnbt.gtf
awk '$3 == "gene"' $tagiso | sed 's/;$//' | awk 'BEGIN {FS=";"} {print $1,$NF}' | sed 's/\t/,/g' | cut -d',' -f 1,4,5,7,9 | sed -E 's/gene_id "([^"]+)".*feelnc_gene_biotype "([^"]+)"/\1,\2/' | awk -F, 'BEGIN{OFS=","} {print $0, "Gene"}' > $dir/nodes_genes_for_import.csv
```

Count the number of transcripts by gene and get Ensembl identifier:

```bash
tagiso=/work/project/fragencode/workspace/geneswitch/results/rnaseq/gallus_gallus/nf-feelnc.tagiso.23-03-03/results/annotation/novel.clean.withgnbt.gtf
python3 /home/ocarpentier/save/script/count_transcript_by_gene.py $tagiso /home/ocarpentier/work/result/tagiso/transcripts_by_gene.csv
``` 

Add a header: `chromosome,start:int,end:int,strand,name:ID,biotype,:LABEL`

```bash
sed -i '1ichromosome,start:int,end:int,strand,name:ID,biotype,:LABEL' $dir/nodes_genes_for_import.csv
```

Ouput file:

CSV containing:
- Chromosome
- Start
- End
- Strand
- GeneID
- Biotype (mRNA, lncRNA, noOrf, TUCp)
- Label

#### Mirdeep miRNA

##### Novel

Select only mirdeep unknown and create file for importation
```bash
mirdeep=/work/project/fragencode/workspace/geneswitch/results/srnaseq/gallus_gallus/nf-core.smrnaseq.1.1.0.GRCg6a.102.21-11-29/mirdeep2/mirdeep/allsamples.75pcentTP.merged.2exp.seenmmquant.mirna.id.coord.constituants.nbexp.status.tsv
awk 'BEGIN {FS="\t"} $6 == "novel" {print $1, $2}' $mirdeep | sed -e 's/:/,/g' -e 's/ /,/g' | awk -F, 'BEGIN{OFS=","} {print $0, "novel","miRNA"}' > $dir/nodes_mirna_novel_for_import.csv
``` 

Add a header: `name:ID,chromosome,start:int,end:int,strand,species,:LABEL`

```bash
sed -i '1iname:ID,chromosome,start:int,end:int,strand,species,:LABEL' $dir/nodes_mirna_novel_for_import.csv
```

Ouput file:

CSV containing:
- MirdeepId
- Chromosome
- Start
- End
- Strand
- Species
- Label

##### Gallus gallus

```bash
mirdeep=/work/project/fragencode/workspace/geneswitch/results/srnaseq/gallus_gallus/nf-core.smrnaseq.1.1.0.GRCg6a.102.21-11-29/mirdeep2/mirdeep/allsamples.75pcentTP.merged.2exp.seenmmquant.mirna.id.coord.constituants.nbexp.status.tsv
grep "known" $mirdeep > /home/ocarpentier/work/result/aggregation/allsamples.75pcentTP.merged.2exp.seenmmquant.mirna.id.coord.constituants.nbexp.status_KNOWN.tsv
```

```bash
awk '/Gallus_gallus/' /home/ocarpentier/work/result/aggregation/allsamples.75pcentTP.merged.2exp.seenmmquant.mirna.id.coord.constituants.nbexp.status_KNOWN.tsv | awk 'BEGIN {FS="\t"} {print $1, $2}'| sed -e 's/:/,/g' -e 's/ /,/g' | awk -F, 'BEGIN{OFS=","} {print $0, "Gallus gallus","miRNA"}' > $dir/nodes_mirna_gallus_for_import.csv
```

Correspondence between mirbase and mirdeep identifiers:

```bash
python3 /home/ocarpentier/save/script/get_ids_mirbase_mirdeep.py -i /home/ocarpentier/work/result/aggregation/allsamples.75pcentTP.merged.2exp.seenmmquant.mirna.id.coord.constituants.nbexp.status_KNOWN.tsv -o /home/ocarpentier/work/result/aggregation/correponding_mirdeep2_mirbase_ids.csv
```

Add mirbase identifiers to file `nodes_mirna_gallus_for_import.csv` and create file for importation:

```bash
join -t $',' -o 1.1,1.2,1.3,1.4,1.5,1.6,1.7,2.2 $dir/nodes_mirna_gallus_for_import.csv /home/ocarpentier/work/result/aggregation/mirdeep_mirbase_ID.csv > $dir/nodes_mirna_gallus_for_import.temp && mv $dir/nodes_mirna_gallus_for_import.temp $dir/nodes_mirna_gallus_for_import.csv
```

Add a header: `name:ID,chromosome,start:int,end:int,strand,species,:LABEL,mirbaseID`

```bash
sed -i '1iname:ID,chromosome,start:int,end:int,strand,species,:LABEL,mirbaseID' $dir/nodes_mirna_gallus_for_import.csv
```

Ouput file:

CSV containing:
- MirdeepId
- Chromosome
- Start
- End
- Strand
- Species
- Label
- MirbaseID

##### Other species

Create file for importation:

```bash
awk '!/Gallus_gallus/' /home/ocarpentier/work/result/aggregation/allsamples.75pcentTP.merged.2exp.seenmmquant.mirna.id.coord.constituants.nbexp.status_KNOWN.tsv | awk 'BEGIN {FS="\t"} {print $1, $2}'| sed -e 's/:/,/g' -e 's/ /,/g' | awk -F, 'BEGIN{OFS=","} {print $0, "other","miRNA"}' > $dir/nodes_mirna_other_for_import.csv
```

Add a header: `name:ID,chromosome,start:int,end:int,strand,species,:LABEL`

```bash
sed -i '1iname:ID,chromosome,start:int,end:int,strand,species,:LABEL' $dir/nodes_mirna_other_for_import.csv
```

Ouput file:

CSV containing:
- MirdeepId
- Chromosome
- Start
- End
- Strand
- Species
- Label

### Relationships files

#### Overlap between peaks and Tagiso genes

To take into account only overlaps with tagiso transcripts:

```bash
tagiso=/work/project/fragencode/workspace/geneswitch/results/rnaseq/gallus_gallus/nf-feelnc.tagiso.23-03-03/results/annotation/novel.clean.withgnbt.gtf
awk '$3 == "transcript"' $tagiso | sed 's/;$//' > /home/ocarpentier/work/result/aggregation/novel.clean.withgnbt.transcript.gtf
```

Find Overlaps:

```bash
peaks=/work/project/fragencode/workspace/geneswitch/results/atacseq/gallus_gallus/nf-core.atacseq.v2.1.2.GRCg6a.108.23-12-15/bwa/merged_replicate/macs2/broad_peak/consensus/consensus_peaks.mRp.clN.bed
python3 /home/ocarpentier/save/overlapping_tss.py $peaks /home/ocarpentier/work/result/aggregation/novel.clean.withgnbt.transcript.gtf 0 gtf /home/ocarpentier/work/result/overlapping/overlapping_peaks_tagiso_gap_0.csv
```
Select only overlapping lines and create file for importation:

```bash
awk '!/-1/' /home/ocarpentier/work/result/overlapping/overlapping_peaks_tagiso_gap_0.csv | cut -d',' -f 6,10 | tail -n +2 | awk -F, 'BEGIN{OFS=","} {print $0, "OVERLAPS_TSS_OF"}'> $dir/relationships_overlapping_peaks_gene_for_import.csv
```

Add a header: `:END_ID,:START_ID,:TYPE`

```bash
sed -i '1i:END_ID,:START_ID,:TYPE' $dir/relationships_overlapping_peaks_genes_for_import.csv
```

Ouput file:

CSV containing the gene ID, the peak name and the relation type

#### Overlap between peaks and miRNAs

Find overlaps:

```bash
peaks=/work/project/fragencode/workspace/geneswitch/results/atacseq/gallus_gallus/nf-core.atacseq.v2.1.2.GRCg6a.108.23-12-15/bwa/merged_replicate/macs2/broad_peak/consensus/consensus_peaks.mRp.clN.bed
mirdeep=/work/project/fragencode/workspace/geneswitch/results/srnaseq/gallus_gallus/nf-core.smrnaseq.1.1.0.GRCg6a.102.21-11-29/mirdeep2/mirdeep/allsamples.75pcentTP.merged.2exp.seenmmquant.mirna.id.coord.constituants.nbexp.status.tsv 
python3 /home/ocarpentier/save/script/overlapping_tss.py $peaks $mirdeep 0 mirdeep /home/ocarpentier/work/result/overlapping/overlapping_peaks_mirdeep_gap_0.csv
```

```bash
awk '!/-1/' /home/ocarpentier/work/result/overlapping/overlapping_peaks_mirdeep_gap_0.csv | cut -d',' -f 5,9 | tail -n +2 | awk -F, 'BEGIN{OFS=","} {print $0, "OVERLAPS_TSS_OF"}'> $dir/relationships_overlapping_peaks_mirna_for_import.csv
```

Add a header: `:END_ID,:START_ID,:TYPE`

```bash
sed -i '1i:END_ID,:START_ID,:TYPE' $dir/relationships_overlapping_peaks_mirna_for_import.csv
```

Ouput file:

CSV containing the miRNA ID, the peak name and the relation type

#### Targetscan

Find correspondences between mirbase identifiers and mirdeep2 identifiers:

```bash
python3 /home/ocarpentier/save/script/get_ids_mirbase_mirdeep.py -i /home/ocarpentier/work/result/aggregation/allsamples.75pcentTP.merged.2exp.seenmmquant.mirna.id.coord.constituants.nbexp.status_KNOWN.tsv -o /home/ocarpentier/work/result/aggregation/correspondences_mirdeep2_mirbase_ids.csv
```
Find correspondences between tagiso transcripts identifiers and tagiso genes identifiers:

```bash
tagiso=/work/project/fragencode/workspace/geneswitch/results/rnaseq/gallus_gallus/nf-feelnc.tagiso.23-03-03/results/annotation/novel.clean.withgnbt.gtf 
awk '$3 == "transcript" {print $10 $12}' $tagiso | sed -e 's/;$//g' -e 's/"//g' -e 's/;/,/g' > /home/ocarpentier/work/result/aggregation/correspondences_tagiso_gene_transcript_ids.csv
```

Aggregation of targetscan results for tagiso genes and mirdeep identifiers:

```bash
targetscan=/work/project/fragencode/workspace/geneswitch/results/multi/mirna-targets/targetscan/gallus_gallus/tagiso.and.mirbase/gallus_gallus.targetscan.out
python3 /home/ocarpentier/save/script/targetscan_aggregation.py -i $targetscan -g /home/ocarpentier/work/result/aggregation/correspondences_tagiso_gene_transcript_ids.csv -m /home/ocarpentier/work/result/aggregation/correspondences_mirdeep2_mirbase_ids.csv -o /home/ocarpentier/work/result/aggregation/targetscan_gene_mirdeep.csv

```

Keep only relationships with a mirdeep identifier (known relationships in the Gallus gallus species):
```bash
awk '!/None/' /home/ocarpentier/work/result/aggregation/targetscan_gene_mirdeep.csv | sort -u > /home/ocarpentier/work/result/aggregation/targetscan_gene_mirdeep_known.csv
```
Filter targetscan results to retain the best target sites:
```bash
python3 home/ocarpentier/save/script/find_best_target_site.py -i /home/ocarpentier/work/result/aggregation/targetscan_gene_mirdeep_known.csv -o /home/ocarpentier/work/result/aggregation/targetscan_gene_mirdeep_known_filtered.csv
``` 

Create file fort importation in Neo4j:
```bash
awk -F, 'BEGIN{OFS=","} {print $0, "TARGETS"}' /home/ocarpentier/work/result/aggregation/targetscan_gene_mirdeep_known_filtered.csv > $dir/relationships_targetscan_for_import.csv
```

Add a header: `:END_ID,:START_ID,type,start:int,end:int,:TYPE`

```bash
sed -i '1i:END_ID,:START_ID,type,start:int,end:int,:TYPE' $dir/relationships_targetscan_for_import.csv
```

#### GENIE3


Retrieve the list of tagiso gene identifiers to filter GENIE3 results:
```bash
cut -d',' -f 1 /home/ocarpentier/work/result/aggregation/correspondences_tagiso_gene_transcript_ids.csv | sort -u > /home/ocarpentier/work/result/genie3/list_genes_id.txt
``` 

Extract score GENIE3 by pair:
```bash
genie3=/work/project/fragencode/workspace/geneswitch/results/multi/rnaseq.srnaseq/gallus_gallus/Genie3/GENIE3_RF_seed123.tsv
python3 /home/ocarpentier/save/script/matrix_to_long_format.py -i $genie3 -o /home/ocarpentier/work/result/genie3/GENIE3_score_filtered.csv -c /home/ocarpentier/work/result/genie3/list_genes_id.txt
```

Create file fort importation in Neo4j:
```bash
awk -F, 'BEGIN{OFS=","} {print $0, "GENIE3_INFERENCE_SCORE"}' /home/ocarpentier/work/result/genie3/GENIE3_score_filtered.csv > $dir/relationships_genie3_for_import.csv
```

Add a header: `:START_ID,:END_ID,score:float,:TYPE`

```bash
sed -i '1i:START_ID,:END_ID,score:float,:TYPE' $dir/relationships_genie3_for_import.csv
```

To exclude null values:

```bash
awk -F, '$3 != "0.0"' $dir/relationships_genie3_for_import.csv > $dir/relationships_genie3_filtered_for_import.csv
```
#### Correlation

Extract correlation value by couple :
```bash
matrix_correlation=/home/ocarpentier/fragencode/workspace/geneswitch/results/multi/rnaseq.srnaseq/gallus_gallus/PCIT/tagiso.and.mirdeep2ok.PCIT/results_cor_matrix.Rdata


Rscript /home/ocarpentier/save/script/extract_cor_PCIT.R -i $matrix_correlation -o /home/ocarpentier/work/result/correlation/exp_correlation.csv
```

Create file fort importation in Neo4j :
```bash
awk -F, 'BEGIN{OFS=","} {print $0, "EXPRESSION_CORRELATION"}' /home/ocarpentier/work/result/correlation/exp_correlation.csv > $dir/relationships_correlation_for_import.csv
```

Add a header: `:START_ID,:END_ID,score:float,:TYPE`

```bash
sed -i '1i:START_ID,:END_ID,value:float,:TYPE' $dir/relationships_correlation_for_import.csv
```

## Database creation

```bash
neo4j-admin database import full --overwrite-destination --nodes=$dir/nodes_genes_for_import.csv --nodes=$dir/nodes_mirna_novel_for_import.csv --nodes=$dir/nodes_peaks_for_import.csv --nodes=$dir/nodes_mirna_gallus_for_import.csv --nodes=$dir/nodes_mirna_other_for_import.csv --relationships=$dir/relationships_overlapping_peaks_mirna_for_import.csv --relationships=$dir/relationships_targetscan_for_import.csv --relationships=$dir/relationships_overlapping_peaks_genes_for_import.csv --relationships=$dir/relationships_genie3_filtered_for_import.csv neo4j --relationships=$dir/relationships_correlation_for_import.csv neo4j--verbose
``` 

## Database access

To change the default password (neo4j):

```bash
neo4j-admin dbms set-initial-password multireg
``` 

To query the database:

```bash
neo4j start
cypher-shell -u neo4j -p multireg
```
This command opens an interactive shell that can be used to run queries againts a Neo4j instance.

## Database update

Create a schema index to speed up queries

```cypher
CREATE INDEX name_gene IF NOT EXISTS FOR (g:Gene) ON (g.name)
```
To load a file from any directory, modify the configuration file `~/work/soft/neo4j/neo4j-community-5.17.0/conf/neo4j.conf`. This line needs to be commented on: `server.directories.import=import`

```cypher
LOAD CSV WITH HEADERS FROM 'file:///home/ocarpentier/work/result/tagiso/transcripts_by_gene.csv' AS file MATCH (g:Gene) WHERE g.name = file.geneID SET g.ensemblID = file.ensemblID SET g.transcript = toInteger(file.transcript)
```

## Archives 

```bash
cd /home/ocarpentier/work/multireg/
```

`multireg_import_files_12_03_2024`:

- relationships_genie3_filtered_for_import.csv: script GENIE3 using `mirdeep2.id.txt`
- relationships_targetscan_for_import.csv: based on overlap between the seeds of target sites (allows overlap between sites if they don't have the same seed and favors the most conserved sites)

Number of relationships: 83,438,768

Number of nodes: 410,604

Number of properties: 107,630,000


`multireg_import_files_19_03_2024`:

- relationships_genie3_filtered_for_import.csv: script GENIE3 using `mirdeep2ok.id.txt`
- relationships_targetscan_for_import.csv: based on overlap between the target sites (favors the most conserved sites)

Number of relationships: 85,750,237

Number of nodes: 410,604

Number of properties: 109,671,337


`multireg_import_files_04_04_2024`:

- relationships_correlation_for_import.csv 
- nodes_genes_for_import.csv : add EnsemblId + number of transcripts

Number of relationships: 

Number of nodes: 410,604

Number of properties: 
