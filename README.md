# M2_Oceane_MultiReg

Océane Carpentier (M2 de Bioinformatique de Rennes IBISa), 
bienvenue dans le stage intitulé 
_Construction et interrogation de graphes de connaissances pour 
l’intégration de données de génomique fonctionnelle_
, co-encadré par Cervin Guyomar d'INRAE et Sarah Djebali de l'INSERM.

Note: la présentation du stage se trouve [ici](https://genoweb.toulouse.inrae.fr/~sdjebali/material/m2/oceane-2024/Presentation_M2_integration_par_graphe.pdf) et le sujet du stage [ici](https://genoweb.toulouse.inrae.fr/~sdjebali/material/m2/oceane-2024/Stage_M2_2024_Multireg.pdf).

Ce document a pour but de te donner des pointeurs vers des ressources pour ton stage. 
Cervin et moi sommes aussi bien entendu à ta disposition
pour répondre à tes questions. Vincent Rocher de MIAT souhaite aussi suivre ton travail, 
nous organiserons des réunions hebdomadaires tous ensemble.

## Documents utiles

A ton arrivée tu peux consulter ces documents qui contiennent beaucoup d'infos, et certaines peuvent être utiles (congés, horaires, accidents...).

- [Le guide d'accueil du centre](https://toulouse.intranet.inrae.fr/media/files/web-guide-accueil-occitanie-toulouse-septembre-2022)
- [Celui du labo (GenPhySE)](https://intra-genphyse.toulouse.inra.fr/Rglement%20intrieur%20et%20Livret%20dAccueil/2022/R%C3%A8glementLivret-GenPhySE-2022-v3_06-07-2022.pdf)

## Les relations régulatrices de l'expression des gènes
Si les différentes cellules d'un organisme complexe comme l'homme ou les animaux d'élevage possèdent toutes le même génome, leur fonctionnement diffère.

Ceci est du à des différences d'expression de gènes entre cellules, elles-mêmes dues à des régulations différentes de l'expression de ces gènes.

Il existe plusieurs régulateurs de l'expression des gènes, qui peuvent agir au moment de la transcription, après la transcription ou même après la traduction en protéine s'il s'agit d'un transcript codant.

Ces régulateurs peuvent être des régions génomiques, comme dans le cas des promoteurs, enhancers, insulateurs, représseurs, ou encore des molécules, comme dans le cas des ARNs longs non codants ou encore des micro ARN (miRNA).

Ici nous nous intéresserons essentiellement aux relations régulatrices entre miRNA et gène codant, les miRNAs étant connus comme des répresseurs des gènes codants, à la fois en empêchant la stabilisation de l'ARN messager (mRNA) en le dégradant, mais aussi en limitant la traduction en protéine de ce dernier en se fixant sur sa partie 4' transcrite non traduite, dite 3'UTR. Il existerait aussi des cas d'activation de l'expression des gènes mais dans une moindre mesure [revue Orang et al, 2014](https://genoweb.toulouse.inrae.fr/~sdjebali/material/m2/oceane-2024/mirnas/orang_internjourngenomics_2014_mirna_review.pdf).

Ce qui est actuellement connu de la biogenèse, fonction et régulation des miRNA est expliqué dans ce récent article de revue du laboratoire d'[E.Lai](https://genoweb.toulouse.inrae.fr/~sdjebali/material/m2/oceane-2024/mirnas/shang_natrevgenet_2023_review_mirna_lai.pdf) mais aussi dans cet [autre article de Bartel de 2018](https://genoweb.toulouse.inrae.fr/~sdjebali/papers/mirnas/reviews/bartel_cell_2018_metazoanmirnas.pdf), en particulier les 6mers, 7mer etc.

Des articles sur leur identification dans les génomes à partir de données NGS [ici](https://genoweb.toulouse.inrae.fr/~sdjebali/material/m2/oceane-2024/mirnas/mirna_detection/) et sur l'identification de leurs cibles (gènes codants) [ici](https://genoweb.toulouse.inrae.fr/~sdjebali/material/m2/oceane-2024/mirnas/mirna_target_prediction/).

## Le projet GENE-SWitCH
Le projet [GENE-SWitCH (GS)](https://www.gene-switch.eu/) est un projet européen H2020 dirigé par INRAE (Jouy-en-Josas) et regroupant plusieurs partenaires européens tels que EBI/EMBL, Roslin Institute, Wageningen University, dont Cervin, Sylvain et moi faisons partie et qui a commencé à l'été 2019 et se termine officiellement fin 2023 (même si les analyses de données de ce projet en sont encore à leur début).

Ce projet a le double but de mieux annoter fonctionnellement les génomes de deux espèces d'intérêt agronomique, le poulet et le porc, et d'aider les éleveurs à mieux sélectionner les animaux sur la base de leur génome (sélection génomique). Cervin, Sylvain et moi sommes surtout impliqué dans le WP2 qui vise au 1er but. Ce stage de M2 pourra ainsi contribuer aux résultats du projet GS. Le projet soumis complet est [ici](https://genoweb.toulouse.inrae.fr/~sdjebali/material/m2/oceane-2024/817998-stage2--SEALED-PROPOSAL.pdf).

Ce projet a déjà généré dans 7 tissus et 3 stades de développement (embryogenèses précoce et tardive et nouveau né) prélevés sur 4 individus (2 males et 2 femelles), tout un pannel de données fonctionnelles haut-débit (RNA-seq d'ARN longs par Illumina et PacBio, RNA-seq d'ARN courts, ATAC-seq pour l'accessibilité de l'ADN, méthylation au promoteur et tout génome, pCHIC sur deux tissus, et plus récemment ChIP-seq sur plusieurs marques d'histones mais dans 3 tissus seulement).

Nous avons déjà fait une analyse de base de ces données (pipeline de traitement depuis les lectures brutes pour identifier les éléments fonctionnels et leur niveau d'activité dans les 84 échantillons, QC, normalisation et analyse différentielle), cependant il reste maintenant à les intégrer, pour une espèce donnée en combinant les techniques mais aussi au travers des espèces (génomique comparative). Une présentation récente de notre groupe de travail INRAE/INSERM Toulouse se trouve [ici](https://docs.google.com/presentation/d/1J6qFp6x1JDC1RbfMbPi3aKRHhWLenquoQfz_9R9zbYA/edit?usp=sharing).

## Méthode proposée pour identifier les relations miRNA/gène codant
Si le but final est de proposer une méthode bioinformatique qui puisse identifier tout type de relation régulatrice de l'expression des gènes (projet multireg), dans ce stage nous nous intéresserons surtout aux relations de type miRNA/gène codant, en utilisant les données fonctionnelles du projet GENE-SWitCH sur le poulet dans un premier temps.

Si des méthodes d'intégration de données fonctionnelles haut-débit de différents types existent, leur but est différent du nôtre. Pour cette raison et aussi pour le caractère intuitif et algorithmiquement fourni des graphes, nous souhaitons utiliser des graphes, plus précisément des multi-graphes orientés colorés, dans lesquels les noeuds sont des objets biologiques (gène codant, miRNA, région accessible de l'ADN) et dans lesquels les arêtes sont des relations simples entre objets (corrélation d'expression sur plusieurs échantillons, proximité génomique, prédiction d'hybridation du miRNA sur le 3'UTR du gène codant, etc).

Après un parcours de la littérature, il existerait un outil nommé [multiRI](https://alpha.dmi.unict.it/~gmicale/MultiRI/) pour manipuler de tels graphes, mais sa publication en est encore à l'état de [preprint](https://genoweb.toulouse.inrae.fr/~sdjebali/material/m2/oceane-2024/methods/micale_2020_multiRI_subgraphmatching_inlabeled_multigraphs.pdf) et le seul exemple d'utilisation qu'il donne est sur un graphe extrêmement simple, il n'est donc pas du tout sûr qu'il fonctionne pour nous, et encore moins qu'il passe à léchelle de nos données qui concernent des dizaines de milliers de gènes et des centaines de milliers de régions accessibles, et encore davantage d'arrêtes.
Il n'est aussi pas tout à fait assez flexible pour nous, puisqu'il ne cherche que des motifs exacts, et ne permet par d'avoir des valeurs quantitatives sur les arrêtes.

Nous avons donc pensé aux graphes de connaissance et en particulier à la base de données de graphe [neo4j](https://neo4j.com/developer/graph-platform) et son langage d'interrogation [cypher](https://neo4j.com/developer/cypher/guide-sql-to-cypher/).

## Aspects techniques (cluster, modules, ...)
Dans le projet GENE-SWitCH nous travaillons tous sur le [cluster de calcul de genotoul](https://www.genotoul.fr/portfolio_page/bioinformatique/) qui est de type slurm et s'appelle maintenant genobioinfo.

Une présentation de ce cluster a récemment été donnée par la plateforme genotoul bioinfo et se trouve [ici](https://genoweb.toulouse.inrae.fr/~sdjebali/material/accueil.inrae/SEMINAIRE_GENOTOUL_BIOINFO_2023.pdf). Un document d'accueil à INRAE sur les bonnes pratiques informatiques se trouve [ici](https://genoweb.toulouse.inrae.fr/~sdjebali/material/accueil.inrae/Accueil_SD.pdf) mais tout ne te concernera pas.

Il ne faut pas lancer de calcul sur le noeud frontal (celui auquel tu accèdes en te loguant en ssh), donc tu as deux possibilités :
- utiliser un noeud interactif en faisant ceci (tu peux aussi demander plusieurs cpus par cette commande)

`srun --x11 --mem=8G --pty bash`
- lancer un job directement sur le cluster, par exemple avec la commande suivante, mais en ayant bien ajouté `#!/bin/bash` en en-tête de script.sh :

`sbatch --mem=8G --cpus-per-task=1 -J <jobname> --mail-user=<email> --mail-type=END,FAIL -D <dirforslurmfiles> --export=ALL -p workq script.sh`


## Données et programmes/codes du projet GENE-SWitCH
Il seront répertoriés dans un google excel [ici](https://docs.google.com/spreadsheets/d/1vmyoZfv7C1wl5TARcPnw7PXXLFH3MCChkhbGlyzy_qo/edit?usp=sharing).

## Missions
Dans un premier temps, il sera utile de faire la bibliographie sur :
- les methodes d'identification de relations miRNA/gene en utilisant au moins RNA-seq des arns longs et RNA-seq des arns courts (ou des miRNA). Pour cela tu peux regarder des revues [ici](https://genoweb.toulouse.inrae.fr/~sdjebali/material/m2/oceane-2024/mirnas/mirna_target_prediction/)
- les bases de donnees de relations miRNA/gene, savoir ce qu'elles utilisent et avoir une idee de leur fiabilite

Puis ou en parallele, tu pourras :
- faire le schema de la base de donnees qui nous interesse
- utiliser neo4J pour contruire cette base de donnees sur donnees réelles GS

Ensuite il s'agira de :
- faire des statistiques de base sur les relations targetscan
- faire des statistiques de base sur les relations genie3
- faire des statistiques de base sur les relations de correlation
- extraire les relations communes entre ces trois ensembles (correlations negatives sous un seuil? voir en fonction des stats precedentes)
- verifier que les scores des methodes individuelles sont meilleures sur ce sous-ensemble
- extraire le sous-ensemble de ces relations qui relient des mirbase et des ensembl qui ont des orthologues chez l'humain
- voir si certaines de ces relations se retrouvent dans mirtarbase

## Pour identifier les relations mirbase/tagiso avec orthologues chez l'humain
Une fois que tu as les relations communes entre les trois methodes (moins que 224k avec les correlations negatives), il faudra : 
- utiliser tous les gènes ensembl 108 associés à un gène tagiso (attention dans la BD tu n'as que le best_ref, dans le fichier tagiso il faut regarder le champ ensembl_gene_id qui contient une liste de gènes ensembl séparés par des virgules)
- utiliser le fichier `/work/project/fragencode/data/species/multi/homo_sapiens-gallus_gallus/ens102_orth_genes_homo_sapiens-gallus_gallus.tsv` pour passer des gènes ensembl du poulet (associés aux gènes tagiso, colonne 2) aux gènes ensembl humains (colonne 1) je sais que c'est la version 102 et non 108, j'ai le fichier pour la 108 mais les identifiants du poulet ne correspondent pas aux nôtres)
- utiliser le fichier `/work/project/fragencode/data/species/gallus_gallus/mirbase/mirbase_gga_hsa_families.tsv` pour passer des identifiants mirbase poulet (colonne 4) aux identifiants mirbase humain (colonne 7)

Attention on utilise ici toutes les relations d'orthologie, pas seulement les relations 1-1 donc il faudra pour un gène ensembl ou mirbase de poulet bien garder tous les gènes ensembl ou mirbase humains qui lui correspondent

Je l'ai fait moi-même pour les 224k relations ici : `/work/project/fragencode/workspace/sdjebali/geneswitch/analysis/multi/multireg/orthology/README.sh` et j'ai vu que l'on perdait surtout au niveau de la derniere étape (demander de l'orhologie chez les mirna)
