#!/usr/bin/env python3

##Author: Océane Carpentier
##Date of creation 15/03/2024

#####################################

# Examples : 
# python3 matrice_to_long_format.py -i GENIE3_RF_seed123.tsv -o GENIE3_long_format.csv
# python3 matrice_to_long_format.py -i GENIE3_RF_seed123.tsv -o GENIE3_long_format_filtered.csv -c list_geneID.txt -l list_mirdeepID.txt

############IMPORT###################
import argparse
import pandas as pd
#####################################

parser = argparse.ArgumentParser(description= 'This program transform a matrix in a long format with the option of filtering matrix entries')

parser.add_argument('-i', type=str, help='Input file : Matrix in TSV format with rownames and colnames', required=True)
parser.add_argument('-c', type=str, help='Identifier to keep in column in TXT format')
parser.add_argument('-o', type=str, help='Output file in CSV format', required=True)
parser.add_argument('-l', type=str, help='Identifier to keep in line in TXT format')


args = parser.parse_args()

input_file = args.i
output_file = args.o
identifier_column = args.c
identifier_line = args.l

#########################################################
df = pd.read_table(input_file)
print(df.shape)
if identifier_column :
    keep_ids = [line.strip() for line in open(identifier_column, 'r')]
    print(len(keep_ids))
    df = df.filter(items=keep_ids, axis=1)
    print(df.shape)
if identifier_line :
    df = df.filter(items=[line.strip() for line in open(identifier_line, 'r')], axis=0)
df = df.reset_index()
melted_df = df.melt(id_vars=['index'], value_name='value')
print(melted_df.shape)
melted_df.to_csv(output_file, index=False,header=False)
