#!/bin/bash

# SLURM options:

#SBATCH --job-name=score_GENIE3
#SBATCH --output=score_GENIE3.log

#SBATCH --cpus-per-task=1
#SBATCH --mem=8G


# env :

source ~/fragencode/tools/modules.genologin.load.2023-11-14.sh

DIR=/home/ocarpentier/work/result/genie3
GENIE3=/work/project/fragencode/workspace/geneswitch/results/multi/rnaseq.srnaseq/gallus_gallus/Genie3/GENIE3_RF_seed123.tsv

cut -d',' -f 1 /home/ocarpentier/work/result/aggregation/correspondences_tagiso_gene_transcript_ids.csv | sort -u > $DIR/list_genes_id.txt


LIST_GENES=/home/ocarpentier/work/result/genie3/list_genes_id.txt


python3 /home/ocarpentier/work/m2_oceane_multireg/scripts/GENIE3/matrix_to_long_format.py -i $GENIE3 -o $DIR/GENIE3_score_filtered.csv -c $LIST_GENES
