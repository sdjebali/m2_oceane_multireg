#!/bin/bash

# SLURM options:

#SBATCH --job-name=count_transcript
#SBATCH --output=count_transcript.log

#SBATCH --cpus-per-task=1
#SBATCH --mem=8G


# env :

source ~/fragencode/tools/modules.genologin.load.2023-11-14.sh

TAGISO=/work/project/fragencode/workspace/geneswitch/results/rnaseq/gallus_gallus/nf-feelnc.tagiso.23-03-03/results/annotation/novel.clean.withgnbt.gtf
DIR=/home/ocarpentier/work/result/aggregation

python3 /home/ocarpentier/work/m2_oceane_multireg/scripts/tagiso/count_transcript_by_gene.py -i $TAGISO -o $DIR/transcripts_by_gene.csv
