#!/usr/bin/env python3

##Author: Océane Carpentier
##Date of creation 03/04/2024
##Last update 10/07/2024

#####################################

# Example : 
# python3 count_transcript_by_gene.py -i novel.clean.withgnbt.gtf -o transcripts_by_gene.csv

############IMPORT###################
import sys
import re
#####################################

parser = argparse.ArgumentParser(description= 'This program counts the number of transcripts per tagiso gene and retrieves the best Ensembl identifier')

parser.add_argument('-i', type=str, help='Input file : Tagiso file', required=True)
parser.add_argument('-o', type=str, help='Output file in CSV format', required=True)

args = parser.parse_args()

input_file = args.i
output_file = args.o

#########################################################

dict_gene_transcript = dict()
with open(input_file, "r") as fh_input :
    with open(output_file, "w") as fh_output :
        fh_output.write("geneID,ensemblID,transcript"+"\n")
        for line in fh_input :
            line_bis = line.strip().split("\t")
            if line_bis[2] =="transcript" :
                gene_id_pattern = r'gene_id\s+"([^"]+)"'
                ensembl_id_pattern = r'best_ref\s+"([^"]+)"'
                gene_id = re.search(gene_id_pattern, line)
                ensembl_id = re.search(ensembl_id_pattern, line) 
                gene_id = gene_id.group(1)
                if ensembl_id is not None :
                    ensembl_id = ensembl_id.group(1)
                if tuple((gene_id,ensembl_id)) not in dict_gene_transcript.keys() :
                    dict_gene_transcript[tuple((gene_id,ensembl_id))] = 1
                else :
                    dict_gene_transcript[tuple((gene_id,ensembl_id))] += 1
        for gene,transcript in dict_gene_transcript.items() :
            fh_output.write(f"{gene[0]},{gene[1]},{transcript}"+"\n")

