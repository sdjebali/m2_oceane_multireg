#!/bin/bash

# SLURM options:

#SBATCH --job-name=sortTargetscanResult
#SBATCH --output=find_best_target_site.log

#SBATCH --cpus-per-task=1
#SBATCH --mem=8G


# env :

source ~/fragencode/tools/modules.genologin.load.2023-11-14.sh

DIR=/home/ocarpentier/work/result/aggregation

awk '!/None/' $DIR/targetscan_gene_mirdeep.csv | sort -u > $DIR/targetscan_gene_mirdeep_known.csv

python3 ~/save/script/find_best_target_site.py -i $DIR/targetscan_gene_mirdeep_known.csv -o $DIR/targetscan_gene_mirdeep_known_filtered.csv
