#!/usr/bin/env python3

##Author: Océane Carpentier
##Date of creation 15/03/2024

#####################################

# Usage : python3 /home/ocarpentier/save/find_best_target_site.py /home/ocarpentier/work/result/aggregation/tagiso_mirdeep_known.csv /home/ocarpentier/work/result/aggregation/tagiso_mirdeep_filtered.csv

# On cluster : sbatch /home/ocarpentier/save/launch_best_target_site.sh 


############IMPORT###################
import argparse
#####################################

parser = argparse.ArgumentParser(description= 'This program filter target site find with TargetScan')

parser.add_argument('-i', type=str, help='Input file : TargetScan result (TSV format without header)', required=True)
parser.add_argument('-o', type=str, help='Output file in CSV format', required=True)


args = parser.parse_args()

input_file = args.i
output_file = args.o

#####################################


def is_overlapping(interval1:tuple,interval2:tuple) -> bool :
    """
    Checks if two intervals overlap.

    Args:
        interval1 (tuple[int, int]): The first interval, represented as a tuple of two integers (start, end).
        interval2 (tuple[int, int]): The second interval, represented as a tuple of two integers (start, end).

    Returns:
        bool: True if the intervals overlap, False otherwise.
    """
    start1, end1 = interval1
    start2, end2 = interval2
    if (start2 <= end1 and start2 >= start1) or (end2 >= start1 and end2 <= end1) :
        return True
    return False


def count_overlaps(site, sites_scores,score_target_type):
    """
    Counts the number of overlaps of a site with other sites, classified by score.

    Args:
        site (tuple): The site to check for overlaps.
        sites_scores (dict): A dictionary mapping sites to their type.
        score_target_type (dict): A dictionary mapping scores to their target types.

    Returns:
        dict: A dictionary of overlap counts for each score type.
    """
    overlaps = {score: 0 for score in set(score_target_type.values())}
    for other in sites_scores:
        if site != other:
            if other[1] < site[0]: 
                continue
            if other[0] > site[1]:
                break 

            if is_overlapping(site, other):
                overlaps[sites_scores[other]] += 1
    return overlaps

def best_site(sites, sites_scores,score_target_type):
    """
    Selects the best site based on the number of overlaps.

    Args:
        sites (set): A set of sites to consider.
        sites_scores (dict): A dictionary mapping sites to their scores.
        score_target_type (dict): A dictionary mapping scores to their target types.

    Returns:
        tuple : The best site based on the criteria of minimal overlaps.
    """
    best = None
    min_overlaps = None
    for site in sites:
        overlaps = count_overlaps(site,sites_scores,score_target_type)
        if sum(overlaps.values()) == 0 :
            return site
        if best is None :
            min_overlaps = overlaps
            best = site
            continue
        count = max(score_target_type.keys())
        # We compare the overlap dictionaries of two sites: 
        # we start by looking at which of the two overlaps the fewest sites 
        # with the maximum score (because we want to keep the maximum number of 
        #high-scoring sites). If the two sites we're comparing overlap by the same number of sites, 
        # we look at the sites with a lower score, and so on.
        while count >= 0 :
            if overlaps[score_target_type.get(count)] < min_overlaps[score_target_type.get(count)] :
                # if the current site has fewer overlaps than the best site, 
                # we stop the loop and this site is considered as the best site.
                best = site
                min_overlaps = overlaps
                break
            if overlaps[score_target_type.get(count)] > min_overlaps[score_target_type.get(count)] :
                # if the current site has a higher number of overlaps than the best site, 
                #we stop the loop and this site cannot be considered as the best one.
                break
            count -= 1
    return best

def select_best_site(dict_sites):
    """
    Selects the best sites based on overlap counts and target types.

    Args:
        dict_sites (dict): A dictionary mapping sites to their target types.

    Returns:
        dict: A dictionary of selected sites.
    """
    score_target_type = {0:"6mer",1:"7mer-1a",2:"7mer-m8",3:"8mer-1a"}
    selected_sites = dict()
    all_sites = set(dict_sites.keys())
    update_sites = dict_sites
    for score in sorted(set(score_target_type.values()), reverse=True):
        current_sites = {site for site in all_sites if dict_sites[site] == score}
        while current_sites:
            if score == score_target_type[min(score_target_type)] :
                # when we reach the lowest-scoring sites, only non-overlapping sites remain, 
                # so we add them directly to the selected sites.
                for site6mer in current_sites :
                    selected_sites[site6mer] = dict_sites[site6mer]
                return selected_sites
            site = best_site(current_sites, update_sites,score_target_type)
            selected_sites[site] = dict_sites[site]
            all_sites = {other for other in all_sites if (site[1] < other[0] or site[0] > other[1])}
            current_sites = {other for other in current_sites if (site[1] < other[0] or site[0] > other[1])}
            update_sites = {key: update_sites[key] for key in all_sites if key in update_sites}
    return selected_sites


def write_file(filename,dict_target) :
    """
    Writes the selected sites and their details to a file.

    Args:
        filename (str): The name of the file to write to.
        dict_target (dict): A dictionary containing the target details to write.
    """
    with open(filename, "w") as fh_output :
        for couple in dict_target :
            for seed in dict_target[couple] :
                fh_output.write(couple[0]+","+couple[1]+","+dict_target[couple][seed]+","+str(seed[0])+","+str(seed[1])+"\n")


if __name__ == "__main__" :
    target_site = dict()
    with open(input_file, "r") as fh_input :
        for line in fh_input : 
            line = line.strip().split(",")
            couple = tuple([line[0],line[1]])
            target_type = line[2]
            site = tuple((int(line[3]),int(line[4])))
            if couple not in target_site.keys():
                target_site[couple] = {site : target_type}
            else :
                target_site[couple][site] = target_type
                
    for couple,dict_sites in target_site.items() :
        sort_dict_sites = {k: dict_sites[k] for k in sorted(dict_sites)} #sort the dictionary by site start position
        dict_sites_filtered = select_best_site(sort_dict_sites)
        target_site[couple] = dict_sites_filtered
    
    write_file(output_file,target_site)











