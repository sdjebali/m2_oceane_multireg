#!/bin/bash

# SLURM options:

#SBATCH --job-name=orthology
#SBATCH --output=orthology_neg_corr.log

#SBATCH --cpus-per-task=1
#SBATCH --mem=64G


# env :

source ~/fragencode/tools/modules.genologin.load.2023-11-14.sh

conda activate jupyter-R

notebook=/home/ocarpentier/work/m2_oceane_multireg/scripts/notebook/orthology_gga_hs.ipynb
output_notebook=/home/ocarpentier/work/m2_oceane_multireg/results/orthology/allrelations_orthology_gga_hs.ipynb
output_dir=/home/ocarpentier/work/result/orthology/all/
relations_dir=/home/ocarpentier/work/result/genie3/GENIE3_score_filtered.csv
output_name=allrelations

mkdir -p $output_dir

papermill $notebook $output_notebook -p output_dir $output_dir -p relations $relations_dir -p output $output_name



