# Organization

- GENIE3/
    - execution_GENIE3_parsed.R
    - matrix_to_long_format.py
    - launch_extract_score_genie3.sh
- aggregation/
    - gets_ids_mirbase_mirdeep.py
    - targetscan_aggregation.py
    - launch_targetscan_aggregation.sh
- correlation/
    - extract_cor_PCIT.R
    - launch_extract_cor_PCIT.sh
- notebook/
    - analysis_correlation.ipynb
    - analysis_genie3.ipynb
    - analysis_targetscan.ipynb
    - comparison_genie3_pcit.ipynb
    - comparison_genie3_threshold1M_targetscan.ipynb
    - enrichment_analysis.ipynb
    - overlapping_peak_gene.ipynbs
- orthology/
    - orthology_gga_hs.ipynb
    - launch_orthology_18k.sh
    - launch_orthology_all.sh
    - launch_orthology_corr_neg.sh
    - launch_orthology_genie3.sh
    - launch_orthology_targetscan.sh
- overlapping/
    - overlapping_tss.py
- tagiso/
    - count_transcript_by_gene.py
    - launch_count_transcript.sh
- targetscan/
    - find_best_target_site.py
    - launch_best_target_site.sh


# GENIE3/

Retrieve the list of tagiso gene identifiers to filter GENIE3 results:
```bash
cut -d',' -f 1 /home/ocarpentier/work/results/aggregation/correspondences_tagiso_gene_transcript_ids.csv | sort -u > /home/ocarpentier/work/results/genie3/list_genes_id.txt
``` 

Extract score GENIE3 by pair:
```bash
genie3=/work/project/fragencode/workspace/geneswitch/results/multi/rnaseq.srnaseq/gallus_gallus/Genie3/GENIE3_RF_seed123.tsv
python3 /home/ocarpentier/save/script/matrix_to_long_format.py -i $genie3 -o /home/ocarpentier/work/results/genie3/GENIE3_score_filtered.csv -c /home/ocarpentier/work/results/genie3/list_genes_id.txt
```

# Aggregation/

Find correspondences between mirbase identifiers and mirdeep2 identifiers:

```bash
python3 /home/ocarpentier/save/script/get_ids_mirbase_mirdeep.py -i /home/ocarpentier/work/results/aggregation/allsamples.75pcentTP.merged.2exp.seenmmquant.mirna.id.coord.constituants.nbexp.status_KNOWN.tsv -o /home/ocarpentier/work/results/aggregation/correspondences_mirdeep2_mirbase_ids.csv
```
Find correspondences between tagiso transcripts identifiers and tagiso genes identifiers:

```bash
tagiso=/work/project/fragencode/workspace/geneswitch/results/rnaseq/gallus_gallus/nf-feelnc.tagiso.23-03-03/results/annotation/novel.clean.withgnbt.gtf 
awk '$3 == "transcript" {print $10 $12}' $tagiso | sed -e 's/;$//g' -e 's/"//g' -e 's/;/,/g' > /home/ocarpentier/work/results/aggregation/correspondences_tagiso_gene_transcript_ids.csv
```

Aggregation of targetscan results for tagiso genes and mirdeep identifiers:

```bash
targetscan=/work/project/fragencode/workspace/geneswitch/results/multi/mirna-targets/targetscan/gallus_gallus/tagiso.and.mirbase/gallus_gallus.targetscan.out
python3 /home/ocarpentier/save/script/targetscan_aggregation.py -i $targetscan -g /home/ocarpentier/work/results/aggregation/correspondences_tagiso_gene_transcript_ids.csv -m /home/ocarpentier/work/results/aggregation/correspondences_mirdeep2_mirbase_ids.csv -o /home/ocarpentier/work/results/aggregation/targetscan_gene_mirdeep.csv

```

# Correlation/

Extract correlation value by couple :
```bash
matrix_correlation=/home/ocarpentier/fragencode/workspace/geneswitch/results/multi/rnaseq.srnaseq/gallus_gallus/PCIT/tagiso.and.mirdeep2ok.PCIT/results_cor_matrix.Rdata


Rscript /home/ocarpentier/save/script/extract_cor_PCIT.R -i $matrix_correlation -o /home/ocarpentier/work/results/correlation/exp_correlation.csv
```

# Orthology/

**Need CSV file containing Gallus gallus tagiso gene identifier and Gallus gallus mirdeep mirna identifier**

Example :

```bash
notebook=/home/ocarpentier/work/m2_oceane_multireg/scripts/notebook/orthology_gga_hs.ipynb
output_notebook=/home/ocarpentier/work/m2_oceane_multireg/results/orthology/allrelations_orthology_gga_hs.ipynb
output_dir=/home/ocarpentier/work/results/orthology/all/
relations_dir=/home/ocarpentier/work/results/genie3/GENIE3_score_filtered.csv
output_name=allrelations


papermill $notebook $output_notebook -p output_dir $output_dir -p relations $relations_dir -p output $output_name

``` 

# Overlapping/
## Tagiso transcripts

To take into account only overlaps with tagiso transcripts:

```bash
tagiso=/work/project/fragencode/workspace/geneswitch/results/rnaseq/gallus_gallus/nf-feelnc.tagiso.23-03-03/results/annotation/novel.clean.withgnbt.gtf
awk '$3 == "transcript"' $tagiso | sed 's/;$//' > /home/ocarpentier/work/results/aggregation/novel.clean.withgnbt.transcript.gtf
```

Find Overlaps:

```bash
peaks=/work/project/fragencode/workspace/geneswitch/results/atacseq/gallus_gallus/nf-core.atacseq.v2.1.2.GRCg6a.108.23-12-15/bwa/merged_replicate/macs2/broad_peak/consensus/consensus_peaks.mRp.clN.bed
python3 /home/ocarpentier/save/overlapping_tss.py $peaks /home/ocarpentier/work/results/aggregation/novel.clean.withgnbt.transcript.gtf 0 gtf /home/ocarpentier/work/results/overlapping/overlapping_peaks_tagiso_gap_0.csv
```

## MiRNAs

Find overlaps:

```bash
peaks=/work/project/fragencode/workspace/geneswitch/results/atacseq/gallus_gallus/nf-core.atacseq.v2.1.2.GRCg6a.108.23-12-15/bwa/merged_replicate/macs2/broad_peak/consensus/consensus_peaks.mRp.clN.bed
mirdeep=/work/project/fragencode/workspace/geneswitch/results/srnaseq/gallus_gallus/nf-core.smrnaseq.1.1.0.GRCg6a.102.21-11-29/mirdeep2/mirdeep/allsamples.75pcentTP.merged.2exp.seenmmquant.mirna.id.coord.constituants.nbexp.status.tsv 
python3 /home/ocarpentier/save/script/overlapping_tss.py $peaks $mirdeep 0 mirdeep /home/ocarpentier/work/results/overlapping/overlapping_peaks_mirdeep_gap_0.csv
```

Analysis : `notebook/overlapping_peak_gene.ipynb`




# Targetscan/

**Need result of `targetscan_aggregation.py`**

Keep only relationships with a mirdeep identifier (known relationships in the Gallus gallus species):
```bash
awk '!/None/' /home/ocarpentier/work/results/aggregation/targetscan_gene_mirdeep.csv | sort -u > /home/ocarpentier/work/results/aggregation/targetscan_gene_mirdeep_known.csv
```
Filter targetscan results to retain the best target sites:
```bash
python3 home/ocarpentier/save/script/find_best_target_site.py -i /home/ocarpentier/work/results/aggregation/targetscan_gene_mirdeep_known.csv -o /home/ocarpentier/work/results/aggregation/targetscan_gene_mirdeep_known_filtered.csv
``` 


# Tagiso/

Count the number of transcripts by gene and get Ensembl identifier:

```bash
tagiso=/work/project/fragencode/workspace/geneswitch/results/rnaseq/gallus_gallus/nf-feelnc.tagiso.23-03-03/results/annotation/novel.clean.withgnbt.gtf
python3 /home/ocarpentier/save/script/count_transcript_by_gene.py $tagiso /home/ocarpentier/work/results/tagiso/transcripts_by_gene.csv
``` 

# Notebook/

## analysis_correlation.ipynb

Notebook R which analysis expression correlation values

## analysis_genie3.ipynb

Notebook R for GENIE3 score analysis and justify the choice of GENIE3 score threshold in the Cypher query (home/ocarpentier/save/scripts/extract_relations_threshold.sh)

## analysis_targetscan.ipynb

Notebook R for TargetScan relationships analysis

## comparison_genie3_pcit.ipynb

Notebook R which compare the two methods of GRN : PCIT and GENIE3

## comparison_genie3_threshold1M_targetscan.ipynb

Notebook R that compares the relationships inferred by GENIE3 and TargetScan with those unique to each method

## enrichment_analysis.ipynb

Notebook R for enrichment analysis

## overlapping_peak_gene.ipynbs

Python notebook to analyze overlap between ATAC-seq peaks and genes