import sys
import pandas as pd
import pyranges as pr
import re

def create_pr_peaks(bed_filename):
    pr_peaks = pr.read_bed(bed_filename)
    #pr_peaks.df['Chromosome'] = pr_peaks.df['Chromosome'].astype('str')
    return pr_peaks

def extract_attributs(line):
    gene_id_pattern = r'gene_id\s+"([^"]+)"'
    transcript_id_pattern = r'transcript_id\s+"([^"]+)"'
    biotype_pattern = r'feelnc_gene_biotype\s+"([^"]+)"'
    gene_id = re.search(gene_id_pattern, line)
    transcript_id = re.search(transcript_id_pattern, line)
    biotype = re.search(biotype_pattern, line)
    if biotype is None :
        biotype_pattern = r'transcript_biotype\s+"([^"]+)"'
        biotype = re.search(biotype_pattern, line)
    gene_id = gene_id.group(1)
    transcript_id = transcript_id.group(1)
    biotype = biotype.group(1)
    return gene_id, transcript_id, biotype

def extract_tss_to_gtf(gtf_filename):
    pd_gene = pd.read_table(gtf_filename, header=None)
    pd_gene.loc[pd_gene[6] == '+', 4] = pd_gene[3]
    pd_gene.loc[pd_gene[6] == '-', 3] = pd_gene[4]
    pd_gene[6] = '+'
    resultats = map(extract_attributs, pd_gene[8].to_list())
    gene_ids, transcript_ids, biotypes = zip(*resultats)
    dict_tss = {"Chromosome": pd_gene[0].to_list(),
                "Start": pd_gene[3].to_list(),
                "End": pd_gene[4].to_list(),
                "Strand": pd_gene[6].to_list(),
                "Name": transcript_ids,
                "Gene" :  gene_ids,
                "Type" : biotypes }
    return dict_tss

def create_pr_genes(dict_tss):
    pr_genes = pr.from_dict(dict_tss)
    return pr_genes


def extract_species(line):
    species_pattern = ["Gallus_gallus","novel"]
    for pattern in species_pattern :
        if re.search(pattern,line) :
            return pattern
    return "other"

def extract_tss_to_mirdeep(mirdeep_filename):
    pd_gene = pd.read_table(mirdeep_filename, header=None)
    coor = pd_gene[1].str.split(':', expand=True)
    #pd_gene.drop(columns=[1], inplace=True)
    coor.loc[coor[3] == '+', 2] = coor[1]
    coor.loc[coor[3] == '-', 1] = coor[2]
    coor[3] = '+'
    species = map(extract_species, pd_gene[2].to_list())
    dict_tss = {"Chromosome": coor[0].to_list(),
                "Start": coor[1].to_list(),
                "End": coor[2].to_list(),
                "Strand": coor[3].to_list(),
                "Name": pd_gene[0],
                "Specie" : species }
    return dict_tss


def find_overlaps(file_peak, file_gene, max_gap=0, file_type='gtf'):
    pr1 = create_pr_peaks(file_peak)
    
    if file_type == "gtf" :
        input = extract_tss_to_gtf(file_gene)
    else :
        input = extract_tss_to_mirdeep(file_gene)
    pr2 = create_pr_genes(input)
    print(pr2)
    result = pr2.join(pr1, slack=max_gap,how='outer')
    return result

if __name__=="__main__" :
    result = find_overlaps(sys.argv[1],sys.argv[2],int(sys.argv[3]), sys.argv[4])
    result.to_csv(sys.argv[5])