#!/bin/bash

# SLURM options:

#SBATCH --job-name=aggregation_targescan
#SBATCH --output=aggregation_targescan.log

#SBATCH --cpus-per-task=1
#SBATCH --mem=20G


# env :

source ~/fragencode/tools/modules.genologin.load.2023-11-14.sh

DIR=/home/ocarpentier/work/result/aggregation
MIRDEEP=/work/project/fragencode/workspace/geneswitch/results/srnaseq/gallus_gallus/nf-core.smrnaseq.1.1.0.GRCg6a.102.21-11-29/mirdeep2/mirdeep/allsamples.75pcentTP.merged.2exp.seenmmquant.mirna.id.coord.constituants.nbexp.status.tsv
TAGISO=/work/project/fragencode/workspace/geneswitch/results/rnaseq/gallus_gallus/nf-feelnc.tagiso.23-03-03/results/annotation/novel.clean.withgnbt.gtf
TARGETSCAN=/work/project/fragencode/workspace/geneswitch/results/multi/mirna-targets/targetscan/gallus_gallus/tagiso.and.mirbase/gallus_gallus.targetscan.out

python3 /home/ocarpentier/work/m2_oceane_multireg/scripts/aggregation/get_ids_mirbase_mirdeep.py -i $MIRDEEP -o $DIR/correspondences_mirdeep2_mirbase_ids.csv

awk '$3 == "transcript" {print $10 $12}' $TAGISO | sed -e 's/;$//g' -e 's/"//g' -e 's/;/,/g' > $DIR/correspondences_tagiso_gene_transcript_ids.csv

python3 /home/ocarpentier/work/m2_oceane_multireg/scripts/aggregation/targetscan_aggregation.py -i $TARGETSCAN -g $DIR/correspondences_tagiso_gene_transcript_ids.csv -m $DIR/correspondences_mirdeep2_mirbase_ids.csv -o $DIR/targetscan_gene_mirdeep.csv


