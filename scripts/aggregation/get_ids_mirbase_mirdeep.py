#!/usr/bin/env python3

##Author: Océane Carpentier
##Date of creation 29/03/2024
##Last update 10/07/2024

#####################################

# Example : get_ids_mirbase_mirdeep.py -i allsamples.75pcentTP.merged.2exp.seenmmquant.mirna.id.coord.constituants.nbexp.status.tsv -o correspondences_mirdeep2_mirbase_ids.csv
# 
############IMPORT###################
import argparse

#####################################

parser = argparse.ArgumentParser(description= 'This program search correspondence between mirdeep and mirbase identifiers for Gallus gallus')

parser.add_argument('-i', type=str, help='Input file : mirdeep2 file', required=True)
parser.add_argument('-o', type=str, help='Output file : CSV file containing mirdeep and mirbase identifier for Gallus gallus', required=True)

args = parser.parse_args()

input_file = args.i
output_file = args.o


#########################################################

with open(input_file, "r") as fh_input :
    with open(output_file, "w") as fh_output :
        for line in fh_input :
            line = line.replace('_',"\t")
            line = line.replace(':',"\t")
            line = line.strip().split("\t")
            mirdeepID = line[0]
            for word in line[1:] :
                if word.startswith("gga") :
                    mirbaseID = word
                    fh_output.write(f'{mirdeepID},{mirbaseID}\n')
                    break
