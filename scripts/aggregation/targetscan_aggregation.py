#!/usr/bin/env python3

##Author: Océane Carpentier
##Date of creation 08/02/2024
##Last update 10/07/2024

#####################################

# Example : python3 targetscan_aggregation.py -i gallus_gallus.targetscan.out -g correspondences_tagiso_gene_transcript_ids.csv -m correspondences_mirdeep2_mirbase_ids.csv -o targetscan_gene_mirdeep.csv
# 
############IMPORT###################
import argparse

#####################################

parser = argparse.ArgumentParser(description= 'This program gives TargetScan results for tagiso genes and mirdeep miRNAs')

parser.add_argument('-i', type=str, help='Input file : Targetscan output', required=True)
parser.add_argument('-g', type=str, help='Csv file containing matches between transcript and gene identifiers', required=True)
parser.add_argument('-m', type=str, help='CSV file containing matches between miRNA identifiers')
parser.add_argument('-o', type=str, help='Output file : CSV file', required=True)

args = parser.parse_args()

input_file = args.i
output_file = args.o
gene_transcript_file = args.g
miRNA_ids_file = args.m

#########################################################

transcriptID_geneID = {}
with open(gene_transcript_file, "r") as fh_input :
    for line in fh_input :
        line = line.strip().split(",")
        transcriptID, geneID = line[1], line[0]
        transcriptID_geneID[transcriptID] = geneID

mirna_correspondingID = {}
with open(miRNA_ids_file, "r") as fh_input :
    for line in fh_input :
        line = line.strip().split(",")
        id1, id2 = line[0], line[1]
        if id2 not in mirna_correspondingID.keys():
            mirna_correspondingID[id2] = [id1]
        else :
            mirna_correspondingID[id2].append(id1)


                    
with open(input_file, "r") as fh_input:
    fh_input.readline()
    with open(output_file, "w") as fh_output :
        for line in fh_input :
            line = line.strip().split("\t")
            transcriptID, miArnID, target_type ,start_site, end_site= line[0], line[1], line[8], line[5],line[6]
            mirdeepID = "None" if miArnID not in mirna_correspondingID.keys() else mirna_correspondingID[miArnID]
            if mirdeepID == "None" :
                fh_output.write(transcriptID_geneID[transcriptID]+","+mirdeepID+","+target_type+","+start_site+","+end_site+"\n")
            else :
                for id in mirdeepID :
                    fh_output.write(transcriptID_geneID[transcriptID]+","+id+","+target_type+","+start_site+","+end_site+"\n")
