#!/bin/bash

# SLURM options:

#SBATCH --job-name=correlation
#SBATCH --output=correlation.log

#SBATCH --cpus-per-task=1
#SBATCH --mem=64G


# env :

source ~/fragencode/tools/modules.genologin.load.2023-11-14.sh

MATRIX_CORR=/home/ocarpentier/fragencode/workspace/geneswitch/results/multi/rnaseq.srnaseq/gallus_gallus/PCIT/tagiso.and.mirdeep2ok.PCIT/results_cor_matrix.Rdata
DIR=/home/ocarpentier/work/result/correlation

Rscript /home/ocarpentier/work/m2_oceane_multireg/scripts/correlation/extract_cor_PCIT.R -i $INPUT -o $DIR/exp_correlation.csv